/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yushu.loancalculator;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class BasicServlet03
 */
@WebServlet(description = "Loan Calculator", urlPatterns = {"/LoanCalculator"})
public class LoanCalculator extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private static final String TITLE = "Loan Calculator";

    /**
     * URLs entered in the address bar or taken from a bookmark are always GET
     * requests
     *
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        // Define the type of content that will appear in the http response
        response.setContentType("text/html");

        try ( 
                // The PrintWriter object allows you to write/transmit HTML to the
                // browser that issued the request.
                PrintWriter out = response.getWriter()) {
            // Now for some ugly code
            out.println("<html>");
            out.println("<head>");
            out.println("<meta charset='utf-8'>");
            out.println("<title>" + TITLE + "</title>");
            out.println("<link rel='stylesheet' href='/LoanCalculator/styles/main.css' type='text/css'/>");
            out.println("</head>");
            out.println("<body><h1>" + TITLE + "</h1>");

            // This form will call the post method
            out.println("<form method='post'>");
            out.println("<table>");
            out.println("<tr>");
            out.println("<td>Principal:</td>");
            out.println("<td><input name='principal'/></td>");
            out.println("<td>$</td>");
            out.println("</tr>");
            out.println("<tr>");
            out.println("<td>Interest Rate(Annual):</td>");
            out.println("<td><input name='interest'/></td>");
            out.println("<td>%</td>");
            out.println("</tr>");
            out.println("<tr>");
            out.println("<td>Amortization:</td>");
            out.println("<td><select name='length'>");
            out.println("<option>5</option>");
            out.println("<option>10</option>");
            out.println("<option>15</option>");
            out.println("<option>20</option>");
            out.println("<option>25</option>");
            out.println("<option>30</option>");
            out.println("</select></td>");
            out.println("<td>Years</td>");
            out.println("</tr>");
            out.println("<tr>");
            out.println("<td>&nbsp;</td>");
            out.println("<td><input type='reset'/>" + "<input type='submit'/></td>");
            out.println("<td>&nbsp;</td>");
            out.println("</tr>");
            out.println("</table>");
            out.println("</form>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    /**
     * For the purpose of this example the form in the GET method uses POST for
     * the submit
     *
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        // Define the type of content that will appear in the http response
        response.setContentType("text/html");

        // Retrieve parameters from the query string
        String principal = request.getParameter("principal");
        String interest = request.getParameter("interest");
        String length = request.getParameter("length");
        Calculator c=new Calculator();
        double payment=c.calculateMoortgage(principal, interest, length);

        try (
                // The PrintWriter object allows you to write/transmit HTML to the
                // browser that issued the request.
                PrintWriter out = response.getWriter()) {
            // more ugly code
            out.println("<html>");
            out.println("<head>");
            out.println("<title>" + TITLE + "</title></head>");
            out.println("</head>");
            out.println("<body><h1>" + TITLE + "</h1>");
            out.println("<table>");
            out.println("<tr>");
            out.println("<td>Principal:</td>");
            out.println("<td>" + principal + "</td>");
            out.println("<td>$</td>");
            out.println("</tr>");
            out.println("<tr>");
            out.println("<td>Interest Rate(Annual):</td>");
            out.println("<td>" + interest + "</td>");
            out.println("<td>%</td>");
            out.println("</tr>");
            out.println("<tr>");
            out.println("<td>Amortization</td>");
            out.println("<td>" + length + "</td>");
            out.println("<td>Years</td>");
            out.println("</tr>");
            
            out.println("<tr>");
            out.println("<td>Monthly Payment:</td>");
            out.println("<td>"+payment+"</td>");
            out.println("<td>$</td>");
            out.println("</tr>");
            out.println("</table>");
//            out.println("<div style='border:1px solid #ddd;"
//                    + "margin-top:40px;font-size:90%'>");
//
//            out.println("Debug Info<br/>");
//            Enumeration<String> parameterNames = request.getParameterNames();
//            while (parameterNames.hasMoreElements()) {
//                String paramName = parameterNames.nextElement();
//                out.println(paramName + ": ");
//                String[] paramValues = request.getParameterValues(paramName);
//                for (String paramValue : paramValues) {
//                    out.println(paramValue + "<br/>");
//                }
//            }
//            out.println("</div>");
            out.println("</body>");
            out.println("</html>");
        }
    }
}
